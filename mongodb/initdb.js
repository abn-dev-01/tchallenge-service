//
// initdb.js
// ===================================================

use admin

db.createUser({
    user: "username",
    pwd: "password",
    roles: [{ role: "readWrite", db: "tchallenge" }]
})

use tchallenge

db.createCollection("accounts")

// ---- INSERT DATA ---- 

db.accounts.insert({
    "_id": ObjectId("5eb92873d35c2d9398b5d8a1"),
    "email": "user@user.com",
    "passwordHash": "$2a$10$wJWbvG77RDEJp90KzTHxfure.84Ee4HbA6L0w/a1v40ArXR8N/CtK",
    "category": "PARTICIPANT",
    "roles": ["PARTICIPANT"],
    "status": "APPROVED",
    "personality": {
        "firstname": null,
        "lastname": null,
        "middlename": null,
        "quickname": "User"
    },
    "participantPersonality": {
        "essay": null,
        "linkedin": null,
        "hh": null,
        "github": null,
        "bitbucket": null,
        "website": null
    },
    "registeredAt": ISODate("2018-07-07T11:07:56.063Z"),
    "createdAt": ISODate("2018-07-07T11:07:56.063Z"),
    "lastModifiedAt": ISODate("2018-07-07T11:07:56.063Z")
})

db.accounts.insert({
        "_id": ObjectId("5eb92873d35c2d9398b5d8a2"),
        "email": "user2@user.com",
        "passwordHash": "$2a$10$wJWbvG77RDEJp90KzTHxfure.84Ee4HbA6L0w/a1v40ArXR8N/CtK",
        "category": "PARTICIPANT",
        "roles": ["PARTICIPANT"],
        "status": "APPROVED",
        "personality": {
            "firstname": null,
            "lastname": null,
            "middlename": null,
            "quickname": "User"
        },
        "participantPersonality": {
            "essay": null,
            "linkedin": null,
            "hh": null,
            "github": null,
            "bitbucket": null,
            "website": null
        },
        "registeredAt": ISODate("2018-07-07T11:07:56.063Z"),
        "createdAt": ISODate("2018-07-07T11:07:56.063Z"),
        "lastModifiedAt": ISODate("2018-07-07T11:07:56.063Z")
})

db.accounts.insert({
        "_id": ObjectId("5eb92873d35c2d9398b5d8a6"),
        "email": "robot@robot.com",
        "passwordHash": "$2a$10$wJWbvG77RDEJp90KzTHxfure.84Ee4HbA6L0w/a1v40ArXR8N/CtK",
        "category": "PARTICIPANT",
        "roles": ["PARTICIPANT"],
        "status": "APPROVED",
        "personality": {
            "firstname": null,
            "lastname": null,
            "middlename": null,
            "quickname": "User"
        },
        "participantPersonality": {
            "essay": null,
            "linkedin": null,
            "hh": null,
            "github": null,
            "bitbucket": null,
            "website": null
        },
        "registeredAt": ISODate("2018-07-07T11:07:56.063Z"),
        "createdAt": ISODate("2018-07-07T11:07:56.063Z"),
        "lastModifiedAt": ISODate("2018-07-07T11:07:56.063Z")
})


